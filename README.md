# RegWhyNew
The idea behind RegWhy is that RegEx is powerful but unreadable.
What if we made a method to make regex more understandable?

For example, which would you rather debug?

			(\()?(\d{3})?(\))? ?\d{3}(\-)?\d{4}

or

	var regWhy=new RegWhyExpression();
	regWhy.AddOptionalValueToSearch(regWhy.Characters.LeftParenthesis);
	regWhy.AddOptionalValueToSearch(regWhy.GetExactCount(regWhy.Characters.AnyDigit,3));
	regWhy.AddOptionalValueToSearch(regWhy.Characters.RightParenthesis);
	regWhy.AddToSearch(regWhy.Characters.OptionalSpace);
	regWhy.AddToSearch(regWhy.GetExactCount(regWhy.Characters.AnyDigit,3));
	regWhy.AddOptionalValueToSearch(regWhy.Characters.Dash);
	regWhy.AddToSearch(regWhy.GetExactCount(regWhy.Characters.AnyDigit,4));
	var matches = regWhy.GetMatches("5202821111 (520)282-1188");

Learn about all of regwhy's [features](https://github.com/dmerson/RegWhyNew/blob/master/Features.md)
Also the human explaination gets a more readable form
regWhey.HumanExplanation.ToString()
	
	Left Parenthesis

	Previous Value \( is optional

	Any digit from 0-9

	Get exactly 3

	Previous Value \d{3} is optional

	Right Parenthesis

	Previous Value \) is optional

	A spacebar or not

	Any digit from 0-9

	Get exactly 3

	Dash symbol

	Previous Value \- is optional

	Any digit from 0-9

	Get exactly 4

	
           
