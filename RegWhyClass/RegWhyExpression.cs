﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace RegWhy
{
    public class RegWhyExpression
    {
        public RegWhyExpression()
        {
            Clear();
            Characters = new ReggWhyExpression.Characters(this);
            Presets = new ReggWhyExpression.Presets(this);
        }

        public enum AppendMethod
        {
            WithParenstheis,
            WithoutParenthesis,
            WithNoMatchingParenthesis,
            WithGrouping
        }

        public enum AppendTo
        {
            Search,
            Replace
        }
        public string SearchPattern { get; set; }
        private bool HasBeenRun { get; set; }
        public StringBuilder HumanLanguageForSearch { get; set; }
        public StringBuilder HumanLanguageForReplace { get; set; }
        private Regex RegEx { get; set; }
        public MatchCollection MatchResults { get; set; }
        public ReggWhyExpression.Characters Characters { get; set; }
        public ReggWhyExpression.Presets Presets { get; set; }
        public string TextToSearch { get; set; }
        public string ReplacePattern { get; set; }

        public void AppendToSearch(string textToAdd)
        {
            SearchPattern += textToAdd;
        }
         public void Append(string valueToAdd = "", AppendTo whichPattern = AppendTo.Search, AppendMethod matching = AppendMethod.WithoutParenthesis, string matchName = "")
         {
             string valueForPattern = "";
             string valueForHumanLanguage = "";
             string whichPatternText = "";
             if (whichPattern == AppendTo.Search)
             {
                 whichPatternText = "Search";
             }
             else
             {
                  whichPatternText = "Replace";
             }
             switch (matching)
             {
                 case (AppendMethod.WithGrouping):
                 {
                     valueForHumanLanguage=(string.Format("Append To {2} matching group {0} for value {1}",  matchName,valueToAdd,whichPatternText));
                     valueForPattern = string.Format("(?<{0}>{1})", matchName, valueToAdd);
                     break;
                 }
                     case (AppendMethod.WithoutParenthesis):
                 {
                    valueForHumanLanguage =  string.Format("Append To {0} non- matching group for value {1}", whichPatternText, valueToAdd);
                     valueForPattern = valueToAdd;
                     break;
                 }
                      case (AppendMethod.WithParenstheis):
                 {
                     valueForHumanLanguage = string.Format("Append with Parenthesis to {0}  group for value {1}", whichPatternText, valueToAdd);
                     valueForPattern = string.Format("({0})", valueToAdd);
                     break;
                 }
                      case (AppendMethod.WithNoMatchingParenthesis):
                 {
                     valueForHumanLanguage = string.Format("Append with non-matching Parenthesis to {0}  group for value {1}", whichPatternText, valueToAdd);
                     valueForPattern = string.Format("(?:{0})", valueToAdd);
                     break;
                 }
                 default:
                 {
                     break;
                 }
             }
             if (whichPattern == AppendTo.Search)
             {
                 SearchPattern += valueForPattern;
                 HumanLanguageForSearch.AppendLine(valueForHumanLanguage); 
                
                 
             }
             else
             {
                  ReplacePattern += valueForPattern;
                 HumanLanguageForReplace.AppendLine(valueForHumanLanguage);
             }
              
        }
        //public void AppendToSearch(string valueToAdd, bool matching, string matchName = "")
        //{
        //    if (matching)
        //    {
        //        HumanLanguageForSearch.AppendLine(string.Format("AppendToSearch matching group {0} for value {1}",
        //            matchName,
        //            valueToAdd));
        //        SearchPattern += string.Format("(?<{0}>{1})", matchName, valueToAdd);
        //    }
        //    else
        //    {
        //        HumanLanguageForSearch.AppendLine(string.Format("AppendToSearch non- matching group for value {1}",
        //            matchName,
        //            valueToAdd));
        //        SearchPattern += string.Format("(?:{0})", valueToAdd);
        //    }
        //}

        public void AppendToReplace(string textToAdd)
        {
            ReplacePattern += textToAdd;
        }

        public void AppendToReplace(string valueToAdd, bool matching, string matchName = "")
        {
            if (matching)
            {
                HumanLanguageForReplace.AppendLine(string.Format("Add To Repalce matching group {0} for value {1}",
                    matchName,
                    valueToAdd));
                ReplacePattern += string.Format("(?<{0}>{1})", matchName, valueToAdd);
            }
            else
            {
                HumanLanguageForReplace.AppendLine(string.Format("Add To Replace non- matching group for value {1}",
                    matchName,
                    valueToAdd));
                ReplacePattern += string.Format("(?:{0})", valueToAdd);
            }
        }

        public string Replace()
        {
            Run();
            var example = RegEx.Replace(TextToSearch, ReplacePattern);
            return example;
        }
         public string Remove()
        {
            Run();
            var example = RegEx.Replace(TextToSearch, "");
            return example;
        }
        public void Clear()
        {
            SearchPattern = "";
            TextToSearch = "";
            ReplacePattern = "";
            HasBeenRun = false;
            HumanLanguageForSearch = new StringBuilder();
            HumanLanguageForReplace=new StringBuilder();
            MatchResults = null;
        }

        public void Run()
        {
            if (!HasBeenRun)
            {
                RegEx = new Regex(SearchPattern);
                HasBeenRun = true;
            }
        }

        public MatchCollection Match(string textToSearch)
        {
            Run();
            SetNewValueForTextToSearchIfNotBlank(textToSearch);
            if (MatchResults == null)
            {
                MatchResults = RegEx.Matches(TextToSearch);
            }

            return MatchResults;
        }

        public string SetAsWord(string word)
        {
            HumanLanguageForSearch.AppendLine(string.Format("Get {0} when it occurs with word boundaries", word));
            return @"\b" + word + @"\b";
        }

        public Match MatchExactOnly(string textToSearch = "")
        {
            SetNewValueForTextToSearchIfNotBlank(textToSearch);
            SearchPattern = string.Format("^{0}$", SearchPattern);
            Run();
            if (MatchResults == null)
            {
                MatchResults = RegEx.Matches(TextToSearch);
            }
            return (MatchResults.Count == 1) ? MatchResults[0] : null;
        }

        public bool HasMatch()
        {
            if (MatchResults.Count > 0)
            {
                return true;
            }
            return false;
        }

        public Match MatchByIndex(int index, string textToSearch = "")
        {
            SetNewValueForTextToSearchIfNotBlank(textToSearch);
            if (MatchResults == null || MatchResults.Count == 0)
            {
                Match(TextToSearch);
            }
            if (null == MatchResults || MatchResults.Count <= index)
            {
                return null;
            }
            return MatchResults[index];
        }

        private void SetNewValueForTextToSearchIfNotBlank(string textToSearch)
        {
            if (!string.IsNullOrEmpty(textToSearch))
            {
                TextToSearch = textToSearch;
            }
        }

        public string MatchGroupByIndexAndGroupName(int index, string groupToGet, string textToSearch = "")
        {
            SetNewValueForTextToSearchIfNotBlank(textToSearch);
            if (MatchResults == null)
            {
                Match(TextToSearch);
            }
            if (null == MatchResults || MatchResults.Count <= index)
            {
                return null;
            }
            var match = MatchResults[index];
            if (match.Groups.Count == 0)
            {
                return null;
            }
            var group = match.Groups[groupToGet].ToString();
            return group;
        }

         public void AppendAnyOfTheseCharacters(List<char> listOfCharacters, AppendTo whichPattern=AppendTo.Search)
        {
            var explained = "Any of these characters";
            var valueReturned = "";
            foreach (var character  in listOfCharacters)
            {
                explained += string.Format(@"{0},", character);
                if (character == '>')
                {
                    valueReturned += @"\>";
                }
                else
                {
                      valueReturned += string.Format("{0}", character);
                }
              
            }
            valueReturned = string.Format("[{0}]", valueReturned.Substring(0, valueReturned.Count()));
             Append(valueReturned, whichPattern);
         

        }
        


        public string AppendMatchWordsThisLengthToSearch(int lengthToGet)
        {
            HumanLanguageForSearch.AppendLine(string.Format("Get words with the length of {0}", lengthToGet));

            return SetAsWord(AppendMatchExactCountToSearch(Characters.AnyLetter, lengthToGet));
        }

        public string AppendDontMatchTheseCharactersToSearch(List<char> stringToExculdeList)
        {
            HumanLanguageForSearch.AppendLine(string.Format("Exclude this list {0}", stringToExculdeList.ToArray()));
            var valueReturned = "";

            foreach (var item  in stringToExculdeList)
            {
                valueReturned += string.Format("{0}|", item);
            }
            return string.Format("[^{0}]", valueReturned.Substring(0, valueReturned.Length - 1));
        }

        public string AppendMatchAnyofTheseStringsToSearch(List<string> literals)
        {
            var explained = "Any of these literals ";
            var valueReturned = "";
            foreach (var literal  in literals)
            {
                explained += string.Format(@"{0},", literal);
                valueReturned += string.Format("{0}|", literal);
            }
            valueReturned = string.Format("({0})", valueReturned.Substring(0, valueReturned.Count() - 1));

            HumanLanguageForSearch.AppendLine(explained);
            return valueReturned;
        }

        public string AppendMatchLiteralWithPluralsToSearch(string valueToConvert) //need to tighten this up
        {
            HumanLanguageForSearch.AppendLine(string.Format("Get {0} with plurals", valueToConvert));
            return string.Format("{0}(s|es)?", valueToConvert);
        }

        public string AppendMatchLiteralWithOrWithoutTitleCaseToSearch(string valueToConvert)
        {
            HumanLanguageForSearch.AppendLine(string.Format("The word{0} with title case or not", valueToConvert));
            if (valueToConvert.Length < 2)
            {
                if (valueToConvert.Length == 1)
                {
                    return string.Format("[{0}|{1}]", valueToConvert.ToUpper(), valueToConvert);
                }
                return "";
            }

            var firstLetterlower = valueToConvert[0].ToString().ToLower();
            var firstLetterUpper = valueToConvert[0].ToString().ToUpper();
            var restOfLetters = valueToConvert.Substring(1);
            return string.Format("[{0}|{1}]{2}", firstLetterUpper, firstLetterlower, restOfLetters);
        }

        public void AppendMatchOptionalValueWithJustParenthesisToSearch(string textToAdd)
        {
            SearchPattern += string.Format("({0})?", textToAdd);
            HumanLanguageForSearch.AppendLine(string.Format("Previous Value {0} is optional", textToAdd));
        }

        public void AppenMatchOptionalValueWithoutGroupingToSearch(string textToAdd)
        {
            SearchPattern += string.Format("(?:{0})?", textToAdd);
            HumanLanguageForSearch.AppendLine(string.Format("Previous Value {0} is optional", textToAdd));
        }

        public string AppendMatchOptionalValueWithGroupingToSearch(string valueToGet)
        {
            return AppendMatchZeroOrOneToSearch(valueToGet);
        }

        public string AppendMatchZeroOrOneToSearch(string valueToGet)
        {
            HumanLanguageForSearch.AppendLine(string.Format("Get zero or more{0}", valueToGet));
            return "(" + valueToGet + ")?";
        }

        public string AppendMatchZeroOrMoreToSearch(string valueToGet)
        {
            HumanLanguageForSearch.AppendLine("Get zero or more");
            return valueToGet + "*";
        }

        public string AppendMatchOneOrMoreToSearch(string valueToGet)
        {
            HumanLanguageForSearch.AppendLine("Get zero or more");
            return valueToGet + "+";
        }

        public string AppenMatchUpToThisNumberToSearch(string valueToGet, int maxNumber)
        {
            HumanLanguageForSearch.AppendLine(string.Format("Get zero to {0}", maxNumber));
            return string.Format("{0}{,{1}", valueToGet, maxNumber);
        }

        public string AppendMatchAtLeastThisManyToSearch(string valueToGet, int minNumber)
        {
            HumanLanguageForSearch.AppendLine(string.Format("Get from {0} or more", minNumber));
            return valueToGet + "{," + minNumber + "}";
        }

        public string AppendMatchWithinRangeToSearch(string valueToGet, int minNumber, int maxNumber)
        {
            HumanLanguageForSearch.AppendLine(string.Format("Get from {0} or more", minNumber));
            return valueToGet + "{" + minNumber + "," + maxNumber + "}";
        }

        public string AppendMatchExactCountToSearch(string valueToGet, int numberToGet)
        {
            HumanLanguageForSearch.AppendLine(string.Format("Get exactly {0}", numberToGet));
            return valueToGet + "{" + numberToGet + "}";
        }

        public string MatchOptionalCharacter(char characterToGet)
        {
            return AppendMatchZeroOrOneToSearch(characterToGet.ToString());
        }

        public string AppendMatchWithNamedGroupToSearch(string matchName, string valueToMatch)
        {
            HumanLanguageForSearch.AppendLine(string.Format("Create a match called {0} for {1}", matchName, valueToMatch));
            return "(?<" + matchName + ">" + valueToMatch + ")";
        }
    }
}