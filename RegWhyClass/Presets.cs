﻿using System;
using System.Linq;

namespace RegWhy
{
    public partial class ReggWhyExpression
    {
        public partial class Presets
        {
            public Presets(RegWhyExpression current)
            {
                RegWhy = current;
            }

            public void SetSearchToAllTheWords()
            {

                RegWhy.AppendToSearch(RegWhy.AppendMatchOneOrMoreToSearch(RegWhy.Characters.AnyLetter));

            }

            public string GetStringRegardlessOfCase(string textToChange)
            {
                RegWhy.HumanLanguageForSearch.AppendLine(String.Format("Get {0} no mater the casing", textToChange));
                var result = "";
                foreach (var character in textToChange.ToCharArray())
                {
                    var upper = character.ToString().ToUpper();
                    var lower = character.ToString().ToLower();
                    if (upper != lower)
                    {
                        result += String.Format("[{0}{1}]", upper, lower);
                    }
                    else
                    {
                        result += character.ToString();
                    }
                }

                return result;
            }
            private RegWhyExpression RegWhy { get; set; }


        }
    }
}