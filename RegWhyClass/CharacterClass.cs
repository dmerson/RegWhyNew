﻿namespace RegWhy
{
    public partial class ReggWhyExpression
    {
        public class Characters
        {
            public Characters(RegWhyExpression current)
            {
                RegWhy = current;
            }

            private RegWhyExpression RegWhy { get; set; }

            public string GetSpecialCharacter(string valueToGet)
            {
                RegWhy.HumanLanguageForSearch.AppendLine(string.Format(@"AppendLiteralToSearchPattern string {0}", valueToGet));
                return string.Format(@"\{0}", valueToGet);
            }

            #region Character Codes

            public string LeftBrace
            {
                get
                {
                    RegWhy.HumanLanguageForSearch.AppendLine("left brace");
                    return @"\{";
                }
            }

            public string Colon
            {
                get
                {
                    this.RegWhy.HumanLanguageForSearch.AppendLine("Colon");
                    return @"\:";
                }

            }
             public string SemiColon
            {
                get
                {
                    this.RegWhy.HumanLanguageForSearch.AppendLine("SeniColon");
                    return @"\;";
                }

            }
            public string RightBrace
            {
                get
                {
                    RegWhy.HumanLanguageForSearch.AppendLine("right brace");
                    return @"\}";
                }
            }

            public string LessThan
            {
                get
                {
                    RegWhy.HumanLanguageForSearch.AppendLine("left angle bracket");
                    return @"<{";
                }
            }

            public string LeftAngleBracket
            {
                get { return LessThan; }
            }

            public string GreaterThan
            {
                get
                {
                    RegWhy.HumanLanguageForSearch.AppendLine("left bracket");
                    return @">}";
                }
            }

            public string Equals
            {
                get
                {
                    this.RegWhy.HumanLanguageForSearch.AppendLine("Equals");
                    return @"\=";
                }

            }

            public string RightAngleBracket
            {
                get { return GreaterThan; }
            }

            public string LeftBracket
            {
                get
                {
                    RegWhy.HumanLanguageForSearch.AppendLine("left bracket");
                    return @"\[";
                }
            }

            public string RightBracket
            {
                get
                {
                    RegWhy.HumanLanguageForSearch.AppendLine("left bracket");
                    return @"]}";
                }
            }

            public string Comma
            {
                get
                {
                    RegWhy.HumanLanguageForSearch.AppendLine("Comma");
                    return @"\,";
                }
            }

            public string FrontSlash
            {
                get
                {
                    RegWhy.HumanLanguageForSearch.AppendLine("Front slash");
                    return @"\/";
                }
            }

            public string BackSlash
            {
                get
                {
                    RegWhy.HumanLanguageForSearch.AppendLine("Back slash");
                    return @"\\";
                }
            }

            public string AnyCharacter
            {
                get
                {
                    RegWhy.HumanLanguageForSearch.AppendLine("Any character");
                    return @"[\s|\S]";
                }
            }

            public string AnyCharacterButNewLine
            {
                get
                {
                    RegWhy.HumanLanguageForSearch.AppendLine("Any character but new line");
                    return @".";
                }
            }

            public string AnyDigit
            {
                get
                {
                    RegWhy.HumanLanguageForSearch.AppendLine("Any digit from 0-9");
                    return @"\d";
                }
            }

            public string AnyNonDigit
            {
                get
                {
                    RegWhy.HumanLanguageForSearch.AppendLine("Any character not a digit");
                    return @"\D";
                }
            }

            public string AnyLetter
            {
                get
                {
                    RegWhy.HumanLanguageForSearch.AppendLine("Any letter");
                    return @"[A-Za-z]";
                }
            }

            public string AnyLowerCaseLetter
            {
                get
                {
                    RegWhy.HumanLanguageForSearch.AppendLine("Any letter");
                    return @"[a-z]";
                }
            }

            public string AnyUpperCaseLetter
            {
                get
                {
                    RegWhy.HumanLanguageForSearch.AppendLine("Any letter");
                    return @"[A-Z]";
                }
            }


            public string AnyAlpanumericOrUnderscore
            {
                get
                {
                    RegWhy.HumanLanguageForSearch.AppendLine("Any alpha numeric or underscore");
                    return @"\w";
                }
            }

            public string NonAlpanumericOrUnderscore
            {
                get
                {
                    RegWhy.HumanLanguageForSearch.AppendLine("Any alpha numeric or underscore");
                    return @"\W";
                }
            }

            public string AnyWhitespace
            {
                get
                {
                    RegWhy.HumanLanguageForSearch.AppendLine("Any whitespace such as space, tab and newline");
                    return @"\s";
                }
            }

            public string NonWhitespace
            {
                get
                {
                    RegWhy.HumanLanguageForSearch.AppendLine(
                        "Any character that is not whitespace such as space, tab and newline");
                    return @"\S";
                }
            }

            public string OptionalSpace
            {
                get
                {
                    RegWhy.HumanLanguageForSearch.AppendLine("A spacebar or not");
                    return @" ?";
                }
            }

            public string Tab
            {
                get
                {
                    RegWhy.HumanLanguageForSearch.AppendLine("Tab");
                    return @"\t";
                }
            }

            public string Return
            {
                get
                {
                    RegWhy.HumanLanguageForSearch.AppendLine("Return key");
                    return @"\r";
                }
            }

            public string NewLine
            {
                get
                {
                    RegWhy.HumanLanguageForSearch.AppendLine("New line feed");
                    return @"\n";
                }
            }

            public string WinowsLineFeed
            {
                get
                {
                    RegWhy.HumanLanguageForSearch.AppendLine("A new lline and return");
                    return @"\r\n";
                }
            }

            public string LeftParenthesis
            {
                get
                {
                    RegWhy.HumanLanguageForSearch.AppendLine("Left Parenthesis");
                    return @"\(";
                }
            }

            public string RightParenthesis
            {
                get
                {
                    RegWhy.HumanLanguageForSearch.AppendLine("Right Parenthesis");
                    return @"\)";
                }
            }

            public string Period
            {
                get
                {
                    RegWhy.HumanLanguageForSearch.AppendLine("Period");
                    return @"\.";
                }
            }

            public string Dot
            {
                get { return Period; }
            }

            public string AtSymbol
            {
                get
                {
                    RegWhy.HumanLanguageForSearch.AppendLine("At symbol");
                    return @"\@";
                }
            }

            public string Dash
            {
                get
                {
                    RegWhy.HumanLanguageForSearch.AppendLine("Dash symbol (-)");
                    return @"\-";
                }
            }

            #endregion
        }
    }
}