﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RegWhy;

namespace RegWhyTests
{
    [TestClass]
    public class AppendTests
    {
        [TestMethod]
        public void VerifyBasicAppendWithGroup()
        {
            var regWhy=new RegWhyExpression();
            regWhy.Append(regWhy.AppendMatchAnyofTheseStringsToSearch(new List<string>(){"new","old"}), RegWhyExpression.AppendTo.Search, RegWhyExpression.AppendMethod.WithGrouping, "Mymatch");
            Assert.AreEqual("(?<Mymatch>(new|old))",regWhy.SearchPattern);
            var result = regWhy.Match("The new world order");
            Assert.AreEqual(1,result.Count);
            Assert.AreEqual("new",result[0].ToString());
            Assert.AreEqual("new",regWhy.MatchGroupByIndexAndGroupName(0,"Mymatch"));
        }
         [TestMethod]
        public void VerifyAppendWithReplace()
        {
            var regWhy=new RegWhyExpression();
            regWhy.Append(regWhy.AppendMatchAnyofTheseStringsToSearch(new List<string>(){"kewel","kool"}), RegWhyExpression.AppendTo.Search, RegWhyExpression.AppendMethod.WithGrouping, "Mymatch");
            Assert.AreEqual("(?<Mymatch>(kewel|kool))",regWhy.SearchPattern);
            var result = regWhy.Match("I am too kewel for kool");
             regWhy.Append("cool", RegWhyExpression.AppendTo.Replace, RegWhyExpression.AppendMethod.WithoutParenthesis);
              Assert.AreEqual(2,result.Count);
              Assert.AreEqual("kewel",result[0].ToString());
            Assert.AreEqual("kewel",regWhy.MatchGroupByIndexAndGroupName(0,"Mymatch"));
             var searchResult = regWhy.Replace();
             Assert.AreEqual("I am too cool for cool",searchResult);
            
        }
    }
}
