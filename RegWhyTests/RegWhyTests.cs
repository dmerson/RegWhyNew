﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RegWhy;

namespace RegWhyTests
{
    [TestClass]
    public class RegWhyTests
    {
        [TestMethod]
        public void TestMethod1()
        {
            var regWhy = new RegWhyExpression();
            regWhy.AppendToSearch(regWhy.Characters.AnyCharacter);
            Assert.AreEqual(@"[\s|\S]",regWhy.SearchPattern);
            Assert.AreEqual("Any character",regWhy.HumanLanguageForSearch.ToString().Trim());
            var textToSearch = "Hello world";
            var matches=regWhy.Match(textToSearch);
            Assert.AreEqual(11,matches.Count);
            Assert.AreEqual("H",regWhy.MatchByIndex(0,textToSearch).ToString());

        }

        [TestMethod]
        public void VerifyBracketNumberRegexValues()
        {
            var regWhy=new RegWhyExpression();
            regWhy.AppendToSearch(regWhy.AppendMatchAtLeastThisManyToSearch("x", 4));
            Assert.AreEqual("x{,4}",regWhy.SearchPattern);
        }
         [TestMethod]
        public void VerifyNamedMatch()
        {
            var regWhy=new RegWhyExpression();
            regWhy.AppendToSearch(regWhy.AppendMatchWithNamedGroupToSearch("a","T"));
            Assert.AreEqual("(?<a>T)",regWhy.SearchPattern);
             var matches = regWhy.Match("Test");
             Assert.AreEqual(1,matches.Count);
             Assert.AreEqual("T",matches[0].Groups[1].ToString());
             var groupValue = regWhy.MatchGroupByIndexAndGroupName( 0, "a","Test");
             Assert.AreEqual("T",groupValue);
        }
        [TestMethod]
        public void VerifyAnyOfTheseCharacters()
        {
            var regWhy=new RegWhyExpression();
            regWhy.AppendAnyOfTheseCharacters(new List<char>(){'a','e'}, RegWhyExpression.AppendTo.Search);
            Assert.AreEqual("[ae]",regWhy.SearchPattern);
             var matches = regWhy.Match("Testa");
             Assert.AreEqual(2,matches.Count);
             
        }
         [TestMethod]
        public void VerifyAnyOfTheseStringLiterals()
        {
            var regWhy=new RegWhyExpression();
            regWhy.AppendToSearch(regWhy.AppendMatchAnyofTheseStringsToSearch(new List<string>(){"GPA",@"G.P.A.","Grade Point Average"}));
            Assert.AreEqual("(GPA|G.P.A.|Grade Point Average)",regWhy.SearchPattern);
             var matches = regWhy.Match("Your G.P.A. or GPA is 3.0");
             Assert.AreEqual(2,matches.Count);
             
        }
        [TestMethod]
        public void VerifyTitleCase()
        {
            var regWhy=new RegWhyExpression();
            regWhy.AppendToSearch(regWhy.AppendMatchLiteralWithOrWithoutTitleCaseToSearch("run"));
            Assert.AreEqual("[R|r]un",regWhy.SearchPattern);
             var matches = regWhy.Match("Run dog run");
             Assert.AreEqual(2,matches.Count);
             
        }
        [TestMethod]
        public void VerifyLiteralsWithPluals()
        {
            var regWhy=new RegWhyExpression();
            regWhy.AppendToSearch(regWhy.AppendMatchLiteralWithPluralsToSearch("run"));
            Assert.AreEqual("run(s|es)?",regWhy.SearchPattern);
             var matches = regWhy.Match("run runs runes");
             Assert.AreEqual(3,matches.Count);
           
             
        }
        [TestMethod]
        public void VerifyEmplid()
        {
            var regWhy=new RegWhyExpression();
            regWhy.AppendAnyOfTheseCharacters(new List<char>(){'0','1','2'}, RegWhyExpression.AppendTo.Search);
            Assert.AreEqual("[012]",regWhy.SearchPattern);
            Assert.AreEqual("Append To Search non- matching group for value [012]",regWhy.HumanLanguageForSearch.ToString().Trim());
            regWhy.AppendToSearch(regWhy.AppendMatchExactCountToSearch(regWhy.Characters.AnyDigit,7));
            Assert.AreEqual(@"[012]\d{7}",regWhy.SearchPattern);
             var matches = regWhy.Match("22050080 12345678 32011234");
             Assert.AreEqual(2,matches.Count);
           
             
        }

        [TestMethod]
        public void VerifyUSAPhone()
        {
            var regWhy=new RegWhyExpression();
            regWhy.AppendToSearch(regWhy.AppendMatchOptionalValueWithGroupingToSearch(regWhy.Characters.LeftParenthesis));
            Assert.AreEqual(@"(\()?",regWhy.SearchPattern);
            regWhy.AppendToSearch(regWhy.AppendMatchOptionalValueWithGroupingToSearch(regWhy.AppendMatchExactCountToSearch(regWhy.Characters.AnyDigit,3)));
            Assert.AreEqual(@"(\()?(\d{3})?",regWhy.SearchPattern);
             regWhy.AppendToSearch(regWhy.AppendMatchOptionalValueWithGroupingToSearch(regWhy.Characters.RightParenthesis));
            Assert.AreEqual(@"(\()?(\d{3})?(\))?",regWhy.SearchPattern);
            regWhy.AppendToSearch(regWhy.Characters.OptionalSpace);
            Assert.AreEqual(@"(\()?(\d{3})?(\))? ?",regWhy.SearchPattern);
            regWhy.AppendToSearch(regWhy.AppendMatchExactCountToSearch(regWhy.Characters.AnyDigit,3));
            Assert.AreEqual(@"(\()?(\d{3})?(\))? ?\d{3}",regWhy.SearchPattern);
            regWhy.AppendToSearch(regWhy.AppendMatchOptionalValueWithGroupingToSearch(regWhy.Characters.GetSpecialCharacter("-")));
            Assert.AreEqual(@"(\()?(\d{3})?(\))? ?\d{3}(\-)?",regWhy.SearchPattern);
            regWhy.AppendToSearch(regWhy.AppendMatchExactCountToSearch(regWhy.Characters.AnyDigit,4));
            Assert.AreEqual(@"(\()?(\d{3})?(\))? ?\d{3}(\-)?\d{4}",regWhy.SearchPattern);
        }

        [TestMethod]
        public void VerifyUsaPhoneWithOptionalMethod()
        {
            var regWhy=new RegWhyExpression();
            regWhy.AppendMatchOptionalValueWithJustParenthesisToSearch(regWhy.Characters.LeftParenthesis);
            Assert.AreEqual(@"(\()?",regWhy.SearchPattern);
             regWhy.AppendMatchOptionalValueWithJustParenthesisToSearch(regWhy.AppendMatchExactCountToSearch(regWhy.Characters.AnyDigit,3));
            Assert.AreEqual(@"(\()?(\d{3})?",regWhy.SearchPattern);
            regWhy.AppendMatchOptionalValueWithJustParenthesisToSearch(regWhy.Characters.RightParenthesis);
            Assert.AreEqual(@"(\()?(\d{3})?(\))?",regWhy.SearchPattern);
            regWhy.AppendToSearch(regWhy.Characters.OptionalSpace);
            Assert.AreEqual(@"(\()?(\d{3})?(\))? ?",regWhy.SearchPattern);
            regWhy.AppendToSearch(regWhy.AppendMatchExactCountToSearch(regWhy.Characters.AnyDigit,3));
            Assert.AreEqual(@"(\()?(\d{3})?(\))? ?\d{3}",regWhy.SearchPattern);
            regWhy.AppendMatchOptionalValueWithJustParenthesisToSearch(regWhy.Characters.Dash);
            Assert.AreEqual(@"(\()?(\d{3})?(\))? ?\d{3}(\-)?",regWhy.SearchPattern);
            regWhy.AppendToSearch(regWhy.AppendMatchExactCountToSearch(regWhy.Characters.AnyDigit,4));
            Assert.AreEqual(@"(\()?(\d{3})?(\))? ?\d{3}(\-)?\d{4}",regWhy.SearchPattern);
            var matches = regWhy.Match("5202821111 (520)282-1188");
            Assert.AreEqual(2,matches.Count);
            var match2 = regWhy.MatchByIndex(1,"5202821111 (520)282-1188");
            Assert.AreEqual("(520)282-1188",match2.ToString());
            Assert.AreEqual(5,match2.Groups.Count);
            Assert.AreEqual("(520)282-1188",match2.Groups[0].ToString());
            Assert.AreEqual("(",match2.Groups[1].ToString());
            Assert.AreEqual("520",match2.Groups[2].ToString());
            Assert.AreEqual(")",match2.Groups[3].ToString());
            Assert.AreEqual("-",match2.Groups[4].ToString());
            

        }
          [TestMethod]
        public void VerifyUsaPhoneWithOptionalNotMatchingMethod()
        {
            var regWhy=new RegWhyExpression();
            regWhy.AppenMatchOptionalValueWithoutGroupingToSearch(regWhy.Characters.LeftParenthesis);
            Assert.AreEqual(@"(?:\()?",regWhy.SearchPattern);
             regWhy.AppenMatchOptionalValueWithoutGroupingToSearch(regWhy.AppendMatchExactCountToSearch(regWhy.Characters.AnyDigit,3));
            Assert.AreEqual(@"(?:\()?(?:\d{3})?",regWhy.SearchPattern);
            regWhy.AppenMatchOptionalValueWithoutGroupingToSearch(regWhy.Characters.RightParenthesis);
            Assert.AreEqual(@"(?:\()?(?:\d{3})?(?:\))?",regWhy.SearchPattern);
            regWhy.AppendToSearch(regWhy.Characters.OptionalSpace);
            Assert.AreEqual(@"(?:\()?(?:\d{3})?(?:\))? ?",regWhy.SearchPattern);
            regWhy.AppendToSearch(regWhy.AppendMatchExactCountToSearch(regWhy.Characters.AnyDigit,3));
            Assert.AreEqual(@"(?:\()?(?:\d{3})?(?:\))? ?\d{3}",regWhy.SearchPattern);
            regWhy.AppenMatchOptionalValueWithoutGroupingToSearch(regWhy.Characters.Dash);
            Assert.AreEqual(@"(?:\()?(?:\d{3})?(?:\))? ?\d{3}(?:\-)?",regWhy.SearchPattern);
            regWhy.AppendToSearch(regWhy.AppendMatchExactCountToSearch(regWhy.Characters.AnyDigit,4));
            Assert.AreEqual(@"(?:\()?(?:\d{3})?(?:\))? ?\d{3}(?:\-)?\d{4}",regWhy.SearchPattern);
            var matches = regWhy.Match("5202821111 (520)282-1188");
            Assert.AreEqual(2,matches.Count);
            var match2 = regWhy.MatchByIndex(1,"5202821111 (520)282-1188");
            Assert.AreEqual("(520)282-1188",match2.ToString());
            Assert.AreEqual(1,match2.Groups.Count);
            Assert.AreEqual("(520)282-1188",match2.Groups[0].ToString());
            Assert.AreEqual("",match2.Groups[1].ToString());
          
            

        }

        [TestMethod]
        public void VerifyMatchingNonMatchingWithWeirdCharacter()
        {
              var regWhy=new RegWhyExpression();
                regWhy.Append("GPA",RegWhyExpression.AppendTo.Search,RegWhyExpression.AppendMethod.WithGrouping,"gMoney");
                regWhy.AppendAnyOfTheseCharacters(new  List<char>(){'<','>'}, RegWhyExpression.AppendTo.Search);
            Assert.AreEqual(@"(?<gMoney>GPA)[<\>]",regWhy.SearchPattern);
            var matches = regWhy.Match("GPA>");
            Assert.AreEqual(1,matches.Count);
            Assert.AreEqual(2,matches[0].Groups.Count);
            Assert.AreEqual("GPA>",matches[0].Groups[0].ToString());
            
            Assert.AreEqual("GPA",matches[0].Groups[1].ToString());
            Assert.AreEqual("GPA",matches[0].Groups["gMoney"].ToString());

            
        }

        [TestMethod]
        public void VerifyExactMatch()
        {
            var regWhy = new RegWhyExpression();
            regWhy.Append(valueToAdd: "Test");
            Assert.AreEqual("Test",regWhy.SearchPattern);
            var result = regWhy.MatchExactOnly("Test");
            Assert.IsNotNull(result);
            regWhy.Clear();
            regWhy.Append("Test", RegWhyExpression.AppendTo.Search);
            result = regWhy.MatchExactOnly("ATest");
            Assert.IsNull(result);
        }

        [TestMethod]
        public void VerifyNestedClassIdea()
        {
            var regWhy = new RegWhyExpression();
            var chars = regWhy.Characters;
            regWhy.AppendToSearch(chars.LeftBracket);
            Assert.AreEqual(regWhy.SearchPattern,@"\[");
        }

        [TestMethod]
        public void VerifyDontMatch()
        {
            var regWhy = new RegWhyExpression();
            
            regWhy.AppendToSearch(regWhy.AppendDontMatchTheseCharactersToSearch(new List<char>(){'a','e','i','o','u'}));
            Assert.AreEqual("[^a|e|i|o|u]",regWhy.SearchPattern);
            var matches = regWhy.Match("I can do it");
            Assert.AreEqual(8,matches.Count);
        }

        [TestMethod]
        public void VerifyWord()
        {
            var regWhy = new RegWhyExpression();
            regWhy.AppendToSearch(regWhy.SetAsWord("test"));
            Assert.AreEqual(@"\btest\b",regWhy.SearchPattern);
            regWhy.Clear();
            regWhy.Presets.SetSearchToAllTheWords();
            var matches = regWhy.Match("This is my story of woe.");
            string shouldBeWoe = regWhy.MatchByIndex(5,"This is my story of woe").ToString();
            Assert.AreEqual(6,matches.Count);
            Assert.AreEqual("This",matches[0].ToString());
            Assert.AreEqual("is",matches[1].ToString());
            Assert.AreEqual("woe",shouldBeWoe);

        }



    }
}
