﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RegWhy;

namespace RegWhyTests
{
    [TestClass]
    public class TestPresets    
    {
        [TestMethod]
        public void TestAllTheWords()
        {
            var regWhy = new RegWhy.RegWhyExpression();
            regWhy.Presets.SetSearchToAllTheWords();
            var matches = regWhy.Match("This is four words.");
            Assert.AreEqual(4,matches.Count);

        }
        [TestMethod]
        public void TestExactLengthWords()
        {
            var regWhy = new RegWhy.RegWhyExpression();
            regWhy.AppendMatchOptionalValueWithGroupingToSearch("test");
            regWhy.AppendToSearch(regWhy.AppendMatchWordsThisLengthToSearch(3));
            Assert.AreEqual(@"\b[A-Za-z]{3}\b",regWhy.SearchPattern);
            var matches = regWhy.Match("One two three four five six");
            Assert.AreEqual(3,matches.Count);
            regWhy.AppendToReplace("Three is a magic number");
            string result = regWhy.Replace();
            Assert.AreEqual("Three is a magic number Three is a magic number three four five Three is a magic number",result);

        }

        [TestMethod]
        public void TestWordDespiteCase()
        {
            var regWhy = new RegWhy.RegWhyExpression();
            regWhy.AppendToSearch(regWhy.Presets.GetStringRegardlessOfCase("Dogs"));
            Assert.AreEqual("[Dd][Oo][Gg][Ss]",regWhy.SearchPattern);
            var matches = regWhy.Match("Dogs are cool but dogs are needy. DOGS!");
            Assert.AreEqual(3,matches.Count);
            regWhy.AppendToReplace("dogs");
            var result = regWhy.Replace();
            Assert.AreEqual("dogs are cool but dogs are needy. dogs!",result);

        }

        [TestMethod]
        public void TestIfThen()
        {
            var regWhy = new RegWhyExpression();
            regWhy.Append(@"(?\()\d{3}(?(1)\)|\b)");
             
            var matches = regWhy.Match(@"(123)");
            Assert.AreEqual(1,matches.Count);
            Assert.AreEqual(2,matches[0].Groups.Count);
            Assert.AreEqual("(123)",matches[0].Groups[0].ToString());
            Assert.AreEqual("(123)",matches[0].Groups[1].ToString());
            //Assert.AreEqual(1,matches[0].Groups.Count);
            // matches = regWhy.Match("aasa.bbb.ccc.ddd");
            // Assert.AreEqual(0,matches.Count);


        }
    }
}
