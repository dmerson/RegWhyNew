﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RegWhy;

namespace RegWhyTests
{
    [TestClass]
    public class ReplaceTest
    {
        [TestMethod]
        public void VerifyBasicReplace()
        {
            var regY=new RegWhy.RegWhyExpression();
            regY.TextToSearch = "Test this out";
            regY.ReplacePattern = "Change";
            regY.SearchPattern = "Test";
            regY.Run();
            string result = regY.Replace();
            Assert.AreEqual("Change this out",result);

        }
         [TestMethod]
        public void VerifyBasicGroupReplace()
        {
            var regY=new RegWhy.RegWhyExpression();
            regY.AppendAnyOfTheseCharacters(new List<char>(){'a','e'}, RegWhyExpression.AppendTo.Search);
            regY.ReplacePattern = @"q";
             regY.TextToSearch = "A thing to eat is good.";
          
            string result = regY.Replace();
            Assert.AreEqual("A thing to qqt is good.",result);

        }
         [TestMethod]
        public void VerifyRemove()
        {
            var regY=new RegWhy.RegWhyExpression();
           
            regY.AppendToSearch(@"\bkinda\b ?");
            
             regY.TextToSearch = "Sushi is kinda good.";
          
            string result = regY.Remove();
            Assert.AreEqual("Sushi is good.",result);

        }
    }
}
