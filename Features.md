#Features

##Human Readable
RegWhy tries to create an english translation.

##Characters are not used with different context
For example, most special characters are used in different ways in different context.
This makes the end result harder to read.

###Characters synbols amed in English with redundant names included
  
  var regWhy=new RegWhy();
  
  regWhy.AddToSearch(regWhy.Characters.EnglishName);

Characters used:
- Comma
- Period	   or Dot
- AtSymbol
- FrontSlash
- BackSlash 
- LeftBracket or LeftAngleBracket
- RightBracket	 or RightAngleBracket
- LessThan
- GreaterThan
- Equals
- LeftBrace
- RightBrace
- Tab
- Return
- NewLine
- WinowsLineFeed
- Dash
- Colon
- SemiColon

##Wildcards say what they are
- AnyDigit
- AnyCharacterButNewLine
- AnyNonDigit
- AnyCharacter
- AnyLetter
- AnyLowerCaseLetter
- AnyUpperCaseLetter
- AnyAlpanumericOrUnderscore
- AnyNonAlpanumericOrUnderscore
- AnyWhitespace
- AnyNonWhitespace
- OptionalSpace

###Functions say what they do and can define named groups


- AddToSearch
- AddToReplace
- Replace
- AddOptionalValueToSearch
- AddOptionalNonMatchingValueToSearch
- Clear
- Run
- GetMatches
- GetExactMatchOnly
- HasMatch
- GetMatchByIndex
- GetMatchGroupByIndex
- Literal
- GetAnyOfTheseCharacters
- GetAnyOfTheseLiteralStrings
- GetLiteralWithPlurals
- GetLiteralWithOrWithoutTitleCase
- GetZeroOrOne
- GetZeroOrMore
- GetOneOrMore
- GetUpToThisNumber
- GetThisNumberOrMore
- GetBetweenThisNumber
- GetExactCount
- GetOptionalCharacter
- GetOptionalString
- CreateNamedMatch
- GetWordsThisLength
- GetStringRegardlessOfCase
- SetSearchToAllTheWords

### You can manipulate the regex directly if RegWhy doesn't have a human readable item for it yet
var regWhy=new RegWhy();
regWhy.RegEx.(This is the RegEx which you can manipuate)

###Coming soon in different languages
Languages coming soon are:
1. JavaScript
2. Python
3. Java
