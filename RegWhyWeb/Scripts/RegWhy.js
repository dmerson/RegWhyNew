﻿(function() {
   
    
    
    
    
     regWhy = function(textToSearch) {
        return {
            appendToFind: function (regYExpressionOrString) {
                var isRegYObject = (regYExpressionOrString.__proto__.constructor.name === "regWhyExpression");
                if (isRegYObject) {
                    this.searchString = this.searchString + regYExpressionOrString.value;
                    this.explaination.push(regYExpressionOrString.explained);
                } else {
                    this.searchString = this.searchString + regYExpressionOrString;
                    this.explaination.push("no explaination given");
                }
               
            },
            startGroup:function(groupName) {
                
            },
            endGroup:function(groundName) {
                
            }       ,
            search: function () {
                that = this;
                this.matchFound = new RegExp(this.searchString, "gi").test(textToSearch);
                if (this.matchFound) {
                    this.results = new RegExp(this.searchString, "gi").exec(textToSearch);
                    return this.results;
                }
                return null;

            },
            matchFound:"",
            searchString: "",
            textToSearch: textToSearch,
            explaination: [],
            results:""
    }    
    }
})();
function regWhyExpression(value,explained) {
    this.value = value;
    this.explained = explained;
    return value;
}

var regYExpressions=$ = {
    specialCharacters: {
        leftBrace: new regWhyExpression("\{", "left brace"),
        rightBrace: new regWhyExpression("\}", "right brace"),
        frontSlash: new regWhyExpression("\/", "front slash"),
        backSlash: new regWhyExpression("\\",'back slash')     ,
        colon: new regWhyExpression("\:", "colon"),
        semicolon: new regWhyExpression("\;", "semicolon"),
        leftBracket: new regWhyExpression("\[", "semicolon"),
        rightBracket: new regWhyExpression("\]", "right bracket"),
        lessThan: new regWhyExpression("\<", "less than"),
        greaterThan: new regWhyExpression("\>", "greater than"),
        leftAngleBracket: new regWhyExpression("\<", "less than"),
        rightAngleBracket: new regWhyExpression("\>", "greater than"),
        equals: new regWhyExpression("\=", "equals sign"),
        test:new regWhyExpression("test","just a test value") , 
        tab: new regWhyExpression("\t", "Tab"),
        returnSymbol: new regWhyExpression("\r", "return key"),
        newLine: new regWhyExpression("\n", "new line"),
        windowsLineFeed: new regWhyExpression("\r\n", "A line for a windows document"),
        leftParenthesis: new regWhyExpression("\(", "Left parenthesis"),
        rightParenthesis: new regWhyExpression("\)", "Right parenthesis"),
        period: new regWhyExpression("\.", "Period"),
        dot: new regWhyExpression("\.", "Period"),
        atSymbol: new regWhyExpression("\@", "At symbol"),
        dash:new regWhyExpression("\-","- character")


    }   ,
   
     exactString:function(strLiteral) {
         new regWhyExpression(strLiteral, "The exact value of " + strLiteral);
     }  ,
    anyDigit: new regWhyExpression("\d", "any digit"),
    nonDigit: new regWhyExpression("\D", "any non digit"),
    anyCharacter: new regWhyExpression("[\s|\S]", "any character"),
    anyCharacterButNewLine: new regWhyExpression(".", "any character"),
    anyLetter: new regWhyExpression("[A-Za-z]", "any letter"),
    anyLowerCaseLetter: new regWhyExpression("[a-z]", "any lower case letter"),
    anyUpperCaseLetter: new regWhyExpression("[a-z]", "any upper case letter"),
    anyAlpanumericOrUnderscore: new regWhyExpression("\w", "any Alpanumeric Or Underscore"),
    nonAlpanumericOrUnderscore: new regWhyExpression("\W", "no alphanumerics or underscore"),
    anyWhitespace: new regWhyExpression("\s", "Any whitespace such as space, tab and newline"),
    nonWhitespace: new regWhyExpression("\S", "Any character that is not whitespace such as space, tab and newline"),
    optionalSpace: new regWhyExpression(" ?", "A spacebar or not")
}
