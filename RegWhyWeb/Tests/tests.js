﻿///<reference path="~/Scripts/RegWhy.js"/>
QUnit.test("basic object test", function (assert) {
    var item = regWhy("this is my test");
    
    item.appendToFind(regYExpressions.specialCharacters.test);
    var result = item.search();
    assert.equal("test", item.searchString);
    assert.equal(1,item.explaination.length)
    assert.equal(1, result.length);
    assert.equal("test", result[0]);
 

});